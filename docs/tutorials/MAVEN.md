## Maven 

### Встановлення на Linux

Переходимо в папку з програмами.  
`cd ~/utils`

Качаємо архів.  
`wget http://apache.ip-connect.vn.ua/maven/maven-3/3.5.2/binaries/apache-maven-3.5.2-bin.zip`

Розархівація  
`unzip apache-maven-3.5.2-bin.zip`

Архів можна видалити 
`rm apache-maven-3.5.2-bin.zip`

Пропишемо, в системі, шлях до виконуваного файлу  
`echo "export PATH=$PATH:/home/user/utils/apache-maven-3.5.2/bin" >> ~/.bashrc`

Запустимо новий термінал і виконаємо  
`mvn --version`

### Створити новий проект

Наберемо в консолі  
`mvn archetype:generate`

Введемо groupId: ua.danit  
    artifactId: new_project  
Все інше виберемо варіант за замовчуванням.

### Зібрати проект

`mvn clean package`
